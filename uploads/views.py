import csv
import random
import string
from django.conf import settings
from django.http import HttpResponse
from accounts.models import Intern,Mentor,User
from uploads.serializers import InternFileUploadSerializer,MentorFileUploadSerializer,InternSerializer,MentorSerializer
from rest_framework import generics
from rest_framework import status
import pandas as pd
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.core.mail import send_mail
from accounts.authenticate import CustomJWTAuthentication
from uploads.constants import (
    INTERN_NAME,
    INTERN_EMAIL,
    DATE_JOINED,
    DURATION,
    MENTOR_EMAIL,
    MENTOR_NAME,
    EMAIL,
    DEPARTMENT
)


class InternUploadFileView(generics.CreateAPIView):
    serializer_class = InternFileUploadSerializer
    authentication_classes = [CustomJWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self,request):
        serializer = InternFileUploadSerializer(data=request.data)
        if serializer.is_valid():
            file = serializer.validated_data['intern_file']
            if file.name.endswith('.csv'):
                file_reader = pd.read_csv(file)
                for _,row in file_reader.iterrows():
                    if not User.objects.filter(email = row[INTERN_EMAIL]).exists():
                        User.objects.create_user(
                            email = row[INTERN_EMAIL],
                            is_intern = True
                        )
                        new_intern_file = Intern(
                            name = row[INTERN_NAME],
                            join_date = row[DATE_JOINED],
                            duration = row[DURATION],
                            mentor_id = row[MENTOR_EMAIL],
                            user_id = row[INTERN_EMAIL],
                        ) 
                    else:
                        new_intern_file = Intern.objects.get(user_id = row[INTERN_EMAIL])
                        if new_intern_file.name != row[INTERN_NAME]:
                            new_intern_file.name = row[INTERN_NAME]
                        if new_intern_file.join_date != row[DATE_JOINED]:
                            new_intern_file.join_date = row[DATE_JOINED]
                        if new_intern_file.duration != row[DURATION]:
                            new_intern_file.duration = row[DURATION]
                        if new_intern_file.mentor_id != row[MENTOR_EMAIL]:
                            new_intern_file.mentor_id = row[MENTOR_EMAIL]
                    new_intern_file.save()
                return Response({"status":"success"},status.HTTP_202_ACCEPTED)
            return Response({"status":"only CSV accepted"},status.HTTP_406_NOT_ACCEPTABLE)
        return Response({"status":"Upload a file"},status.HTTP_404_NOT_FOUND)

class MentorUploadFileView(generics.CreateAPIView):
    serializer_class = MentorFileUploadSerializer
    authentication_classes = [CustomJWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self,request):
        serializer = MentorFileUploadSerializer(data=request.data)
        if serializer.is_valid():
            mentor_file = serializer.validated_data['mentor_file']
            if mentor_file.name.endswith('.csv'):
                mentor_file_reader = pd.read_csv(mentor_file)
                for _,row in mentor_file_reader.iterrows():
                    if not User.objects.filter(email = row[EMAIL]).exists():
                        User.objects.create_user(
                            email = row[EMAIL]
                        )
                        new_mentor_file = Mentor(
                            name = row[MENTOR_NAME],
                            user_id = row[EMAIL],
                            specialization = row[DEPARTMENT],
                        )
                    else:
                        new_mentor_file = Mentor.objects.get(user_id = row[EMAIL])
                        if new_mentor_file.name != row[MENTOR_NAME]:
                            new_mentor_file.name = row[MENTOR_NAME]
                        if new_mentor_file.specialization != row[DEPARTMENT]:
                            new_mentor_file.specialization = row[DEPARTMENT]
                    new_mentor_file.save()
                return Response({"status":"success"},status.HTTP_202_ACCEPTED)
            return Response({"status":"only CSV accepted"},status.HTTP_406_NOT_ACCEPTABLE)
        return Response({"status":"Upload a file"},status.HTTP_404_NOT_FOUND)


class MentorDetails(APIView):
    authentication_classes = [CustomJWTAuthentication]
    permission_classes = [IsAuthenticated]


    def get(self,request):
        mentor_details = Mentor.objects.filter(mentors__isnull=False).distinct()
        email = request.query_params.get('email')
        if email:
            interns = Intern.objects.filter(mentor__user=email)
            serializer = InternSerializer(interns,many=True)
            return Response(serializer.data)
        serializer = MentorSerializer(mentor_details,many=True)
        return Response(serializer.data)

class InternDetails(APIView):
    authentication_classes = [CustomJWTAuthentication]
    permission_classes = [IsAuthenticated]

    
    def get(self,request):
        intern_details = Intern.objects.all()
        serializer = InternSerializer(intern_details,many=True)
        return Response(serializer.data)

class GeneratePassword(APIView):
    authentication_classes = [CustomJWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self,request):
        for user in request.data:
            user_data = user
            password = ''.join(random.choices(string.ascii_letters + string.digits,k=8))
            print(password)

            user  = User.objects.get(email=user_data['email'])
            user.set_password(password)
            user.save()

            subject = 'Your Credentials'
            message = f'Hello \n\n Your username is: {user_data["email"]},\n\n your password is: {password}'
            from_email = settings.EMAIL_HOST_USER
            recipient_list = [user_data['email']]
            send_mail(subject,message,from_email,recipient_list,fail_silently=False)
        return Response({"status":"Success"})


class DownloadInternCSV(APIView):
    def get(self,request):
        response = HttpResponse(
            content_type = "text/csv",
            headers = {"Content-Disposition":'attachment;filename="Intern.csv"'},
        )
        writer = csv.writer(response)
        writer.writerow([INTERN_NAME,DATE_JOINED,DURATION,MENTOR_EMAIL,INTERN_EMAIL])

        return response
    
class DownloadMentorCSV(APIView):
    def get(self,request):
        response = HttpResponse(
            content_type = "text/csv",
            headers = {"Content-Disposition":'attachment;filename="Mentor.csv"'},
        )
        writer = csv.writer(response)
        writer.writerow([MENTOR_NAME,EMAIL,DEPARTMENT])
        return response