
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from accounts.models import Intern 
from accounts.models import Mentor
from accounts.models import User
from uploads.serializers import InternSerializer
from uploads.serializers import MentorFileUploadSerializer
from uploads.serializers import InternFileUploadSerializer
from uploads.serializers import MentorSerializer
import io
from django.core.files import File
from django.urls import reverse
import csv
from rest_framework_simplejwt.tokens import AccessToken

class InternMentorTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(email='testuser@example.com', password='testpassword')
        self.access_token = str(AccessToken.for_user(self.user))

    def test_intern_file_upload(self):
        file_path = "Intern.csv"
        with open(file_path, 'rb') as file:
            response = self.client.post(reverse('intern'), {'intern_file': file}, 
                                        HTTP_AUTHORIZATION=f'Bearer {self.access_token}')
            self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    def test_mentor_file_upload(self):
        file_path = "Mentor.csv"
        with open(file_path, 'rb') as file:
            response = self.client.post(reverse('mentor'), {'mentor_file': file}, 
                                        HTTP_AUTHORIZATION=f'Bearer {self.access_token}')
            self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)        

        
    def test_download_interncsv_file(self):
        
        response = self.client.get(reverse('download_intern_csv'), HTTP_AUTHORIZATION=f'Bearer {str(self.access_token)}')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['Content-Type'], 'text/csv')

    def test_download_mentorcsv_file(self):
        
        response = self.client.get(reverse('download_mentor_csv'), HTTP_AUTHORIZATION=f'Bearer {str(self.access_token)}')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['Content-Type'], 'text/csv')  
    


        
