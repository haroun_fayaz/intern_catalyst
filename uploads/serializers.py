from rest_framework import serializers
from accounts.models import Intern,Mentor

class InternFileUploadSerializer(serializers.Serializer):
    intern_file = serializers.FileField()

class MentorFileUploadSerializer(serializers.Serializer):
    mentor_file = serializers.FileField()


class InternSerializer(serializers.ModelSerializer):
     intern_email = serializers.EmailField(source='user')
     mentor_email = serializers.EmailField(source='mentor')
     join_date = serializers.DateField(format='%b-%d-%Y')
     class Meta:
         model = Intern
         fields = ['id','name','intern_email','join_date','duration','mentor_email']

class MentorSerializer(serializers.ModelSerializer):
     mentor_email = serializers.EmailField(source='user')
     class Meta:
         model = Mentor
         fields = ['id','name','mentor_email','specialization']
 