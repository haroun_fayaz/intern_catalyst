from django.urls import path
from uploads.views import InternUploadFileView
from uploads.views import MentorUploadFileView
from uploads.views import MentorDetails
from uploads.views import GeneratePassword
from uploads.views import InternDetails
from uploads.views import DownloadInternCSV
from uploads.views import DownloadMentorCSV

urlpatterns = [
    path('interncsv/', InternUploadFileView.as_view(), name='intern'),
    path('mentorcsv/', MentorUploadFileView.as_view(), name='mentor'),
    path('interns/', InternDetails.as_view(), name='intern-details'),
    path('mentors/', MentorDetails.as_view(), name='mentor-details'),
    path('generatepassword/', GeneratePassword.as_view(), name='generate-password'),
    path('intern/download/csv/',DownloadInternCSV.as_view(),name='download_intern_csv'),
    path('mentor/download/csv/',DownloadMentorCSV.as_view(),name='download_mentor_csv'),
]
