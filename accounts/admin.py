from django.contrib import admin
from .models import User , Mentor, Intern 

class MentorAdmin(admin.ModelAdmin):
    list_display =("name", "specialization")


class InternAdmin(admin.ModelAdmin):
    list_display =("name", "join_date", "duration","mentor")


# class UserAdmin(admin.ModelAdmin):
#     list_display =("name", "intern", "specialization")

admin.site.register(User)
admin.site.register(Intern,InternAdmin)
admin.site.register(Mentor,MentorAdmin)
