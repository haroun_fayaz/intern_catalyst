from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render 
from django.contrib.auth import authenticate 
from rest_framework.response import Response
from rest_framework import status
from . serializers import UserLoginSerializer
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from django.conf import settings
from rest_framework.decorators import authentication_classes,permission_classes
from rest_framework.permissions import AllowAny




def Dashboard(request):
  template = loader.get_template('dashboard.html')
  return HttpResponse(template.render())

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }

@authentication_classes([])
@permission_classes([AllowAny])
class UserLoginView(APIView):
  def post(self, request,format= None):
    serializer = UserLoginSerializer(data=request.data) 
    if serializer.is_valid(raise_exception=False):
       email = serializer.data.get('email') 
       password = serializer.data.get('password')
       user = authenticate(username=email, password=password) 
       if user is not None:
           data = get_tokens_for_user(user)
           response = Response()
           response.set_cookie(
                    key = settings.SIMPLE_JWT['AUTH_COOKIE'], 
                    value = data["access"],
                    expires = settings.SIMPLE_JWT['ACCESS_TOKEN_LIFETIME'],
                    secure = settings.SIMPLE_JWT['AUTH_COOKIE_SECURE'],
                    httponly = settings.SIMPLE_JWT['AUTH_COOKIE_HTTP_ONLY'],
                    samesite = settings.SIMPLE_JWT['AUTH_COOKIE_SAMESITE']
                )
           user_data = {
                    'id': user.id,
                    'email': user.email,
                    'is_superuser': user.is_superuser,
                    'is_intern':user.is_intern,
                }
           response_data = {
                    'msg': 'Login Success',
                    'user': user_data,
                    'data' : data,
                                }
           
           response.data =response_data
           return response
       else:
        return Response({'errors':{'non_field_errors':['Email or Password is not Valid']}}, status=status.HTTP_404_NOT_FOUND)
        
    return Response (serializer.errors,status=status.HTTP_400_BAD_REQUEST)
      
