from datetime import date
from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.utils import timezone
from dateutil.relativedelta import relativedelta


class CustomUserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    def _create_user(self, email, password=None, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)
class User(AbstractUser):
    username = None
    email = models.EmailField(verbose_name='email', max_length=255, unique=True,)
    is_intern = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    def __str__(self):
      return self.email

    def has_perm(self, perm, obj=None):
      "Does the user have a specific permission?"
      # Simplest possible answer: Yes, always
      return self.is_superuser

    def has_module_perms(self, app_label):
      "Does the user have permissions to view the app `app_label`?"
      # Simplest possible answer: Yes, always
      return True



#defined choice classes externaly so these can be used outside of the specific class 
class Duration(models.TextChoices):
    ONE_MONTH = '1 Month'
    THREE_MONTHS = '3 Months'
    SIX_MONTHS = '6 Months'
    NINE_MONTHS = '9 Months'
    TWELVE_MONTHS = '12 Months'

    @staticmethod
    def duration_in_months(duration_string):
        return int(duration_string.split()[0])


    class Meta:
        db_table = 'duration'


class Department(models.TextChoices):
    FRONTEND = 'Frontend'
    BACKEND = 'Backend'
    DEVOPS = 'Devops'
    ANDROID = 'Android'
    HR = 'Hr'


    class Meta:
        db_table = 'department'


class Mentor(models.Model):
    name = models.CharField(max_length=30)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True,to_field='email',db_column='mentor_email')
    specialization = models.CharField(choices=Department.choices , max_length=130) # add choices frontend , backend , hr ,devops using choice field


    def __str__(self):
        return self.name
    

    class Meta:
        db_table = 'Mentor'


class Intern(models.Model):
    name = models.CharField(max_length=30)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True,to_field='email',db_column='intern_email')
    join_date = models.DateField(default=timezone.now)
    duration = models.CharField(choices=Duration.choices,max_length=130, default=0)
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE,null=True, related_name='mentors',to_field='user',db_column='mentor_email')

    def __str__(self):
        return self.name
    
    
    class Meta:
        db_table = 'Intern'

    @property
    def is_active_intern(self):
        duration_in_months =  Duration.duration_in_months(self.duration)
        end_date = self.join_date + relativedelta(months=duration_in_months)
        return end_date >= date.today()
    
