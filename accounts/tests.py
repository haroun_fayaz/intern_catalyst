import datetime
from urllib import response
from django.conf import settings
from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework.test import APIClient
from rest_framework import status

from accounts.serializers import UserLoginSerializer

from accounts.models import Mentor
from accounts.models import Intern
from accounts.models import User


from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model

from rest_framework import status
from rest_framework.test import APIClient

from rest_framework_simplejwt.tokens import RefreshToken
from datetime import timedelta

User = get_user_model()

class UserLoginViewTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(
            email='test@test.com', password='testpassword', is_active=True
        )
        
        self.valid_payload = {
            'email': 'test@test.com',
            'password': 'testpassword',
        }
        
        self.invalid_payload = {
            'email': 'test@test.com',
            'password': 'invalidpassword',
        }
    
    
    def test_valid_credentials_login_with_cookies(self):
        response = self.client.post(
            reverse('login'), data=self.valid_payload
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in response.data['data'])
        self.assertTrue('refresh' in response.data['data'])
        self.assertEqual(response.data['user']['id'], self.user.id)
        self.assertTrue(settings.SIMPLE_JWT['AUTH_COOKIE'] in response.cookies)
        self.assertEqual(
            response.cookies[settings.SIMPLE_JWT['AUTH_COOKIE']].value,
            response.data['data']['access'],
        )

    def test_invalid_credentials_login_with_cookies(self):
        response = self.client.post(
            reverse('login'), data=self.invalid_payload
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
 



