# Generated by Django 4.2 on 2023-05-10 11:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_alter_task_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='url',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='status',
            field=models.CharField(choices=[('to_do', 'To Do'), ('in_progress', 'Inprogress'), ('done', 'Done'), ("<class 'dashboard.models.Progress.Meta'>", 'Meta')], default=0, max_length=55),
        ),
    ]
