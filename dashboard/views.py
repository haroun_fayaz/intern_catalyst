import csv
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from dashboard.serializers import TaskFileUploadSerializer, TaskSerializer
from dashboard.models import Task
from accounts.models import Intern,Mentor
from rest_framework import generics
import pandas as pd
from dashboard.constants import (
    TITLE,
    DESCRIPTION,
    ASSIGNED_TO,
    COMPLETION_DATE,
    URL,
    COMMENT,
)

class TaskView(APIView):
    serializer_class = TaskSerializer

    def get_queryset(self):
        mentor_emails = self.request.query_params.getlist('mentors')
        intern_emails = self.request.query_params.getlist('interns')

        if mentor_emails:
            return self.get_tasks_assigned_by_mentor(mentor_emails)
        elif intern_emails:
            return self.get_tasks_assigned_to_intern(intern_emails)
        else:
            return Task.objects.all()

    def get_tasks_assigned_by_mentor(self, mentor_emails):
        mentors = Mentor.objects.filter(user_id__in=mentor_emails)
        return Task.objects.filter(assigned_by__in=mentors)

    def get_tasks_assigned_to_intern(self, intern_emails):
        interns = Intern.objects.filter(user_id__in=intern_emails)
        return Task.objects.filter(assigned_to__in=interns)

    def get(self, request, pk=None):
        if pk:
            task = get_object_or_404(Task, pk=pk)
            serializer = TaskSerializer(task)
            return Response(serializer.data)
        else:
            queryset = self.get_queryset()
            serializer = TaskSerializer(queryset, many=True)
            return Response(serializer.data)
        
    def post(self, request):
        request.data['assigned_by_email'] = request.user.email
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        task = Task.objects.get(pk=pk)
        request.data['assigned_by_email'] = request.user.email
        request.data['assigned_to_email'] = request.user.email

        
        if not task.can_be_updated:
            return Response({'error': 'Task cannot be updated after completion date'}, status=status.HTTP_403_FORBIDDEN)

        serializer = TaskSerializer(task, data=request.data,request=request)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        task = Task.objects.get(pk=pk)
        task.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TaskFileUploadView(generics.CreateAPIView):
    serializer_class = TaskFileUploadSerializer

    def post(self, request):
        serializer = TaskFileUploadSerializer(data=request.data)
        if serializer.is_valid():
            file = serializer.validated_data['task_file']
            if file.name.endswith('.csv'):
                file_reader = pd.read_csv(file,na_values='')
                mentor = Mentor.objects.get(user=request.user)
                for _, row in file_reader.iterrows():
                    intern_email = row[ASSIGNED_TO]
                    if Intern.objects.filter(user__email=intern_email).exists():
                        intern = Intern.objects.get(user__email=intern_email)
                        title = row[TITLE]
                        description = row[DESCRIPTION]
                        completion_date = row[COMPLETION_DATE]
                        url = row[URL]
                        comment = row[COMMENT]
                        if pd.isna(url):
                            url = None
                        if pd.isna(comment):
                            comment = None
                        defaults = {'assigned_to':intern, 'description': description, 'completion_date': completion_date,'url':url,'comment':comment}
                        Task.objects.update_or_create(
                            title=title, assigned_by= mentor, defaults=defaults
                        )
                return Response({"status": "success"})
            return Response({"status": "only CSV accepted"})
        return Response({"status": "Upload a file"})


class DownloadTaskCSV(APIView):
    def get(self,request):
        response = HttpResponse(
            content_type = "text/csv",
            headers = {"Content-Disposition":'attachment;filename="Task.csv"'},
        )
        writer = csv.writer(response)
        writer.writerow([TITLE,DESCRIPTION,ASSIGNED_TO,COMPLETION_DATE,URL,COMMENT])
        return response
    