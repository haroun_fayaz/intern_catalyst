from django.urls import path
from dashboard.views import TaskView,TaskFileUploadView,DownloadTaskCSV

urlpatterns = [
    path('tasks/', TaskView.as_view(), name='create_task'),
    path('tasks/<int:pk>/', TaskView.as_view(), name='update_task'),
    path('tasks/csv/',TaskFileUploadView.as_view(),name='upload_task'),
    path('task/download/csv/',DownloadTaskCSV.as_view(),name='download_csv'),

]