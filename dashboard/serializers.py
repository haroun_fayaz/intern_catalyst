from rest_framework import serializers
from accounts.models import Intern, Mentor
from .models import Task

class TaskSerializer(serializers.ModelSerializer):
    assigned_to = serializers.SerializerMethodField()
    assigned_by = serializers.SerializerMethodField()
    assigned_to_email = serializers.EmailField(write_only=True)
    assigned_by_email = serializers.EmailField(write_only=True)
    completion_date = serializers.DateField(format='%b-%d-%Y',required=False)

    class Meta:
        model = Task
        fields = ['id', 'title', 'description', 'assigned_to','assigned_to_email','assigned_by_email', 'assigned_by', 'status', 'created_on', 'completion_date', 'url','comment']
        # extra_kwargs = {'title': {'required': False}}
    def get_assigned_to(self, task):
        if task.assigned_to:
            return {
                'name': task.assigned_to.name,
                'email': task.assigned_to.user_id
            }
        else:
            return None

    def get_assigned_by(self, task):
        if task.assigned_by:
            return {
                'name': task.assigned_by.name,
                'email': task.assigned_by.user_id
            }
        else:
            return None
    def create(self, validated_data):
        assigned_to_email = validated_data.pop('assigned_to_email', None)
        assigned_by_email = validated_data.pop('assigned_by_email', None)

        if assigned_to_email:
            try:
                assigned_to = Intern.objects.get(user_id=assigned_to_email)
            except Intern.DoesNotExist:
                raise serializers.ValidationError({'assigned_to_email': 'Invalid email address for assigned_to'})
            validated_data['assigned_to'] = assigned_to

        if assigned_by_email:
            try:
                assigned_by = Mentor.objects.get(user_id=assigned_by_email)
            except Mentor.DoesNotExist:
                raise serializers.ValidationError({'assigned_by_email': 'Invalid email address for assigned_by'})
            validated_data['assigned_by'] = assigned_by

        return super().create(validated_data)
    
    
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

        if self.request and self.request.method == 'PUT':
            self.fields['title'].required = False
 

class TaskFileUploadSerializer(serializers.Serializer):
    task_file = serializers.FileField()

