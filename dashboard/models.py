from django.db import models
from django.utils import timezone
from accounts.models import Intern, Mentor

class Progress(models.TextChoices):
    TO_DO = 'to_do'
    INPROGRESS= 'in_progress'
    DONE = 'done'
    
    class Meta:
        db_table = 'progress'
def default_created_on():
    return timezone.now().date()

class Task(models.Model):
    title = models.CharField(max_length=255) 
    description = models.TextField(null=True,blank=True)
    assigned_to = models.ForeignKey(Intern, on_delete=models.CASCADE, null=True,to_field='user',related_name='tasks_assigned')
    assigned_by = models.ForeignKey(Mentor, on_delete=models.CASCADE, null= True,to_field='user',related_name='tasks_created')
    status = models.CharField(choices=Progress.choices,max_length=55, default='to_do')
    created_on = models.DateField(default=default_created_on)
    completion_date = models.DateField(default=default_created_on)
    url = models.URLField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    resource = models.TextField(null=True,blank=True)

    def __str__(self):
       return self.title
    
    class Meta:
        db_table = 'Task'

    @property
    def can_be_updated(self):
        return self.completion_date >= timezone.now().date()
