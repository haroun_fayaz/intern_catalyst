TASK_CSV_HEADERS = ["Title", "Description", "Assigned To", "Completion Date", "Url", "Comment"]

TITLE = "Title"
DESCRIPTION = "Description"
ASSIGNED_TO = "Assigned To"
COMPLETION_DATE = "Completion Date"
URL = "Url"
COMMENT = "Comment"
