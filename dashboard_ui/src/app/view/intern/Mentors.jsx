import React from 'react'
import MUIDataTable from "mui-datatables";
import axios from "../../Axios";
import { LoadingButton } from "@mui/lab";


export  const Mentors = () => {
  const [selectedRows, setSelectedRows] = React.useState([]);
  const [internData, setInternData] = React.useState([]);
  const fetchInterns =async ()=>{
    try {
      const response = await axios.get('uploads/mentors/');
      console.log('data', response.data);
      setInternData(response.data)
    } catch (error) {
      console.error(error);
    }
  }
  React.useEffect(() => {
    fetchInterns();
  }, []);
  
  const columns = [
    {
     name: "name",
     label: "Name",
     options: {
      filter: true,
      sort: true,
     }
    },
    {
     name: "mentor_email",
     label: "Email",
     options: {
      filter: true,
      sort: false,
     }
    },
    {
     name: "specialization",
     label: "Specialization",
     options: {
      filter: true,
      sort: false,
     }
    },
   
   ];
   


  console.log("selectedRows",selectedRows)
   const options = {
     filterType: "dropdown",
     selectableRows: "multiple",
     download: false,
     print: false,
     onRowSelectionChange: (
         _currentRowsSelected,
         _allRowsSelected,
         rowsSelected
       ) => {
        setSelectedRows(rowsSelected);
        }

   };
   const generatePassword = async () => {
    let emails=selectedRows.map((row) =>({ email: internData[row].mentor_email}))
    console.log("email",emails);
 
    try {
      const response = await axios.post('uploads/generatepassword/', emails);
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  return (<>

    {selectedRows.length > 0 && (
        <LoadingButton
          color="primary"
          variant="contained"
          sx={{ my: 2 }}
          onClick={generatePassword}
        >
          Generate Password
        </LoadingButton>
      )}
    <MUIDataTable
      title="Mentor List"
      data={internData}
      columns={columns}
      options={options}
      getData={fetchInterns}
    />
    </>
  );
}

