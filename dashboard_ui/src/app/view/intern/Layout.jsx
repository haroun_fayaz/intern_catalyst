import React from "react";
import TabLayout from "../../components/TabLayout";
import { useLocation} from "react-router-dom";


import {Interns} from "./Interns";
import {Mentors} from "./Mentors";


const Layout = () => {

  const location = useLocation();


  const tabs = [
    {
      label: "Interns",
      component: (
        <Interns />
      ),
    },
    { label: "Mentors", component: <Mentors /> },
  ];

  return (
    <>
      <div className="m-sm-30">
        <TabLayout tabs={tabs} tab={location?.tab} />
      </div>
    </>
  );
};

export default Layout;
