import Loadable from '../../components/Loadable';
import { lazy } from 'react';
// import { authRoles } from '../../auth/authRoles';
// import Interns from './interns';

const Layout = Loadable(lazy(() => import('./Layout')));

const internRoutes = [
  { 
    path: '/interns',
   element: <Layout />, 
  // auth: authRoles.admin
 },
];

export default internRoutes;
