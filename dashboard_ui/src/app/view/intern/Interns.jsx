import React from "react";
import MUIDataTable from "mui-datatables";
import axios from "../../Axios";
import { LoadingButton } from "@mui/lab";

export const Interns = () => {
  const [selectedRows, setSelectedRows] = React.useState([]);
  const [internData, setInterData] = React.useState([]);
  const fetchInterns = async () => {
    try {
      const response = await axios.get("uploads/interns/");
      console.log("data", response.data);
      setInterData(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  React.useEffect(() => {
    fetchInterns();
  }, []);

  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "intern_email",
      label: "Email",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "join_date",
      label: "Join Date",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "duration",
      label: "Duration",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "mentor_email",
      label: "Mentor",
      options: {
        filter: true,
        sort: false,
      },
    },
  ];


  const options = {
    filterType: "dropdown",
    selectableRows: "multiple",
    download: false,
    print: false,
    // searchPosition: 'center',
    textLabels: {
      filter: {
        title: "Filters",
        reset: "Reset",
        apply: "Apply",
      },
    },
    onRowSelectionChange: (
      _currentRowsSelected,
      _allRowsSelected,
      rowsSelected
    ) => {
     console.log("_allRowsSelected,",_allRowsSelected)
     console.log("rowsSelected,",rowsSelected)
     setSelectedRows(rowsSelected);
     }
  };
  const generatePassword = async () => {
    let emails=selectedRows.map((row) =>({ email: internData[row].intern_email }))
    console.log("email",emails);
    try {
      const response = await axios.post('uploads/generatepassword/', emails);
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <>
      {selectedRows.length > 0 && (
        <LoadingButton
          color="primary"
          variant="contained"
          sx={{ my: 2 }}
          onClick={generatePassword}
        >
          Generate Password
        </LoadingButton>
      )}
      <MUIDataTable
        title="Intern List"
        data={internData}
        columns={columns}
        options={options}
        getData={fetchInterns}
      />
    </>
  );
};
