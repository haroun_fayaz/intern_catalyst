import React,{ useState } from "react";
import MUIDataTable from "mui-datatables";
import axios from "../../Axios";
import { Button } from "@mui/material";
import { ShowTaskDialog } from "./ShowTaskDialog"
import {localUser} from "../../util"
import CircularProgress from '@mui/material/CircularProgress';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

const MyTasks = () => {
  const [showDialog, setShowDialog] = useState(false);
  const [selecteditem, setSelectedItem] = useState([]);
  const [taskData, setTaskData] = useState([]);
  const [loading, setLoading] = useState(false);
const [snackbarOpen, setSnackbarOpen] = useState(false);



const handleSnackbarClose = () => {
  setSnackbarOpen(false);
};
  // const [user, setUser] = React.useState({});

  // let { user } = useAuth();
  const handleClose = () => {
    setShowDialog(false);
    setSelectedItem({});
  };
  console.log("User", localUser());
  // const fetchTasks = async () => {
  //   try {
  //     const response = await axios.get("tasks/", {
  //       params: {
  //         interns: localUser().email
  //       },
  //     });
  //     console.log("data", response.data);
  //     setTaskData(response.data);
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };
  const fetchTasks = async () => {
    try {
      setLoading(true); // Start the loader
      const response = await axios.get("tasks/", {
        params: {
          interns: localUser().email
        },
      });
      console.log("data", response.data);
      setTaskData(response.data);
      setLoading(false); // Stop the loader
      setSnackbarOpen(true); // Open the Snackbar on success
    } catch (error) {
      console.error(error);
      setLoading(false); // Stop the loader on error
    }
  };
  React.useEffect(() => {
    // setUser(JSON.parse(window.localStorage.getItem("user")));
    fetchTasks();
  }, []);
  const columns = [
    {
      name: "title",
      label: "Title",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "status",
      label: "Status",
      options: {
        filter: true,
        sort: false,
      },
    },
    // {
    //   name: "assigned_to.name",
    //   label: "assigned_to",
    //   options: {
    //     filter: true,
    //     sort: false,
    //   },
    // },
    {
      name: "description",
      label: "Description",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "completion_date",
      label: "Completion Date",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "",
      label: "Action",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (index, data) => {
          return (
            <Button
              variant="outlined"
              type="submit"
              onClick={() => {
                console.log(
                  "internData[data.rowIndex",
                  taskData[data.rowIndex]
                );
                setSelectedItem(taskData[data.rowIndex]);
                setShowDialog(true);
              }}
            >
              View
            </Button>
          );
        },
      },
    },
  ];
  const options = {
    filterType: "dropdown",
    selectableRows: "none",
    download: false,
    print: false,
    // onRowClick: (rowData, rowMeta, values) => {
    //   console.log("rowData",rowData)
    //   console.log("rowMeta",rowMeta)
    //   console.log("values",values)
    //   // navigate("/cuttings", { state: { data: taskData[rowMeta.rowIndex]}});
    //   navigate(`/show-task/${1}`)
    // },
  };
  return (
    <>
     {loading && <CircularProgress />}
      <MUIDataTable
        title="My Tasks"
        data={taskData}
        columns={columns}
        options={options}
        getData={fetchTasks}
      />
          {showDialog && (
        <ShowTaskDialog  selectedItem={selecteditem} handleClose={handleClose} open={showDialog} />
      )}
     
      <Snackbar
  open={snackbarOpen}
  autoHideDuration={6000}
  onClose={handleSnackbarClose}
  anchorOrigin={{ vertical: "top", horizontal: "right" }}
>
  <MuiAlert onClose={handleSnackbarClose} severity="success" elevation={6} variant="filled">
    Tasks fetched successfully!
  </MuiAlert>
</Snackbar>
    </>
  );
};

export default MyTasks;
