import Loadable from '../../components/Loadable';
import { lazy } from 'react';
// import { authRoles } from '../../auth/authRoles';
// import Interns from './interns';

const MyTasks = Loadable(lazy(() => import('./MyTasks')));
const ShowTasks = Loadable(lazy(() => import('./ShowTaskDialog')))

const InternViewRoutes = [
  { 
    path: '/intern_view',
   element: <MyTasks />, 
  // auth: authRoles.admin
 },
//  { 
//   path: '/show_task',
//  element: <ShowTasks />, 
// // auth: authRoles.admin
// },
];

export default InternViewRoutes;