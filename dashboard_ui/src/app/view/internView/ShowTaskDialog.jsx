import React,{useState} from "react";
import FormDialog from "../../components/FormDialog";
import { Formik, Form } from "formik";
import { Grid } from "@mui/material";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import axios from "../../Axios";
import { Box } from "@mui/material";
import Button from "@mui/material/Button";
import { TASK_STATUS } from "../../Constant"
import * as Yup from "yup";
export const ShowTaskDialog = ({ handleClose, open, selectedItem }) => {
  const [selectedTask,setSelectedTask]=useState([])
  console.log("selectedItems", selectedItem);
  const checkUrl = /^((ftp|http|https):\/\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+((\/)[\w#]+)*(\/\w+\?[a-zA-Z0-9_]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/gm;
  const validationSchema = Yup.object().shape({
    url: Yup.string().matches(checkUrl,'Enter valid Url')
    // url: Yup.string()
    //   .url('Invalid URL')
  });

  const fetchTask = async () => {
    try {
      const response = await axios.get(`tasks/${selectedItem.id}/`);
      console.log("data", response.data);
      setSelectedTask(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  
  React.useEffect(() => {
    fetchTask();
  }, [])
  const initialValues = {
    // title: selectedItem?.title,
    // description: selectedItem?.description,
    // completion_date: selectedItem?.completion_date,
    // resource:selectedItem.resource,
    status: selectedTask?.status,
    url: selectedTask?.url,
    comment: selectedTask?.comment,
  };
  const updateTask = async (values) => {
    try {
      const response = await axios.put(`tasks/${selectedItem.id}/`, values);
      console.log("File uploaded:", response.data);
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <>
      <FormDialog
        title={`Task : ${selectedItem?.title}`}
        open={open}
        handleClose={handleClose}
        // fullWidth
      >
        {/* <Box p={4} height="100%"> */}
        <Formik
          onSubmit={(values, actions) => {
            console.log("on submit", values);
            actions.setSubmitting(false);
            handleClose();
            updateTask(values);
          }}
          // onSubmit={handleFormSubmit}
          initialValues={initialValues}
          validationSchema={validationSchema}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <Form onSubmit={handleSubmit}>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "space-between",
                }}
              >
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      label="Add Url"
                      name="url"
                      type="url"
                      value={selectedTask?.url}
                      fullWidth
                      onChange={handleChange}
                      error={errors.url && touched.url}
                      helperText={errors.url && touched.url ? errors.url : ""}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      // id="outlined-select-currency"
                      select
                      label="Status"
                      name="status"
                      fullWidth
                      value={selectedTask?.status}
                      defaultValue="to_do"
                      sx={{mt: "10px" }}
                      onChange={handleChange}
                      // helperText="Assign To Intern"
                    >
                      {TASK_STATUS.map((option) => (
                        <MenuItem key={option} value={option}>
                          {option}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Add Comment"
                      name="comment"
                      fullWidth
                      multiline
                      rows={3}
                      value={selectedTask?.comment}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      type="submit"
                      color="secondary"
                      variant="contained"
                      
                    >
                      Submit
                    </Button>
                  </Grid>
                </Grid>
              </Box>
            </Form>
          )}
        </Formik>
        {/* </Box> */}
      </FormDialog>
    </>
  );
};
