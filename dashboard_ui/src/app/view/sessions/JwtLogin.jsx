import { LoadingButton } from "@mui/lab";
import { Card, TextField, Typography } from "@mui/material";
import { Box, styled } from "@mui/system";
import useAuth from "../../hooks/useAuth";
import { Formik } from "formik";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";

const FlexBox = styled(Box)(() => ({ display: "flex", alignItems: "center" }));

const JustifyBox = styled(FlexBox)(() => ({ justifyContent: "center" }));

const JWTRoot = styled(JustifyBox)(() => ({
  background: "url(/assets/images/session_frame.png)",
  minHeight: "100vh !important",
  "& .card": {
    maxWidth: 500,
    minHeight: 400,
    margin: "1rem",
    display: "flex",
    borderRadius: 12,
    alignItems: "center",
  },
}));

// inital login credentials
const initialValues = {
  email: "",
  password: "",
};

// form field validation schema
const validationSchema = Yup.object().shape({
  password: Yup.string()
    .min(5, "Password must be 6 character length")
    .required("Password is required!"),
  email: Yup.string()
    .email("Invalid Email address")
    .required("Email is required!"),
});

const JwtLogin = () => {
  const [errorMessage, setErrorMessage] = useState(false);
  const { login } = useAuth();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  const handleFormSubmit = async (values) => {
    setLoading(true);
    try {
      await login(values.email, values.password);
      navigate("/");
    } catch (e) {
      setErrorMessage(e.errors.non_field_errors[0]);
      setLoading(false);
    }
  };

  return (
    <JWTRoot>
      <Card className="card">
        <Box p={4} height="100%">
          <Formik
            onSubmit={handleFormSubmit}
            initialValues={initialValues}
            validationSchema={validationSchema}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
            }) => (
              <form onSubmit={handleSubmit}>
                <Box style={{ textAlign: "center" }}>
                  <img src="/assets/images/logoX.png" alt="logo" width="50" />
                  <Typography variant="h5" mb="2rem">
                    Intern Catalyst
                  </Typography>
                  <Typography variant="h6" mb="1rem">
                    Login
                  </Typography>
                </Box>
                <TextField
                  fullWidth
                  size="small"
                  type="email"
                  name="email"
                  label="Email"
                  onBlur={handleBlur}
                  // value={values.email}
                  onChange={handleChange}
                  helperText={touched.email && errors.email}
                  error={Boolean(errors.email && touched.email)}
                  sx={{ mb: 3 }}
                  autoComplete='off'
                />

                <TextField
                  fullWidth
                  size="small"
                  name="password"
                  type="password"
                  label="Password"
                  onBlur={handleBlur}
                  // value={values.password}
                  onChange={handleChange}
                  helperText={touched.password && errors.password}
                  error={Boolean(errors.password && touched.password)}
                  sx={{ mb: 1.5 }}
                  autoComplete='off'
                />
                {errorMessage && <p style={{ color: "red" }}>{errorMessage}</p>}

                <LoadingButton
                  type="submit"
                  color="primary"
                  loading={loading}
                  variant="contained"
                  sx={{ my: 2 }}
                >
                  Login
                </LoadingButton>
              </form>
            )}
          </Formik>
        </Box>
      </Card>
    </JWTRoot>
  );
};

export default JwtLogin;
