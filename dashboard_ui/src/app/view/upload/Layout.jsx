import React from "react";
import TabLayout from "../../components/TabLayout";
import { useLocation} from "react-router-dom";


import InternUpload from "./InternUpload";
import MentorUpload from "./MentorUpload";


const Layout = () => {

  const location = useLocation();


  const tabs = [
    {
      label: "Interns",
      component: (
        <InternUpload />
      ),
    },
    { label: "Mentors", component: <MentorUpload /> },
  ];

  return (
    <>
      <div className="m-sm-30">
        <TabLayout tabs={tabs} tab={location?.tab} />
      </div>
    </>
  );
};

export default Layout;
