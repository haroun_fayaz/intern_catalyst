import React from "react";
import Dropzone from "../../components/Dropzone";
import axios from "../../Axios";
import { Button,Typography } from "@mui/material";
import UploadFileIcon from "@mui/icons-material/UploadFile";

export default function MentorUpload() {
  const [selectedFile, setSelectedFile] = React.useState(null);

  const handleFileUpload = async () => {
    const formData = new FormData();
    formData.append("mentor_file", selectedFile);

    try {
      const response = await axios.post("uploads/mentorcsv/", formData);
      console.log("File uploaded:", response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const handleFileSelection = (files) => {
    if (files.length > 0) {
      setSelectedFile(files[0]);
    } else {
      setSelectedFile(null);
    }
  };
  return (
    <>
     <Typography variant="h2">Upload CSV file of Mentors</Typography>
      <Dropzone
        dropzoneText="Drag and drop a CSV file here or click"
        Icon={UploadFileIcon}
        filesLimit={1}
        acceptedFiles={["text/csv"]}
        onDrop={handleFileSelection}
        showFileNames
      />
        <div style={{display: "flex",
    justifyContent: "flex-end"}}>
      <Button
        variant="contained"
        color="primary"
        disabled={!selectedFile}
        onClick={handleFileUpload}
      >
        Upload
      </Button>
      </div>
    </>
  );
}
