import React from "react";
import MUIDataTable from "mui-datatables";
import axios from "../../Axios";
import { DialogAssign } from "./DialogAssign";


export const AssignedInterns = () => {
  const [selecteditem, setSelectedItem] = React.useState([]);
  const [showDialog, setShowDialog] = React.useState(false);
  const [internData, setInterData] = React.useState([]);
  const user = JSON.parse(window.localStorage.getItem("user"));
  const fetchInterns = async () => {
    try {
      const response = await axios.get("uploads/mentors/",{
          params: {
            email: user.email,
          }
      });
      console.log("data", response.data);
      setInterData(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  React.useEffect(() => {
    fetchInterns();
  }, []);
  const handleClose = () => {
    setShowDialog(false);
    setSelectedItem({});
  };

  const columns = [
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "intern_email",
      label: "Email",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "join_date",
      label: "Join Date",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "duration",
      label: "Duration",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "mentor_email",
      label: "Mentor",
      options: {
        filter: true,
        sort: false,
      },
    },
    // {
    //   name: "",
    //   label: "Action",
    //   options: {
    //     filter: true,
    //     sort: false,
    //     customBodyRender: (index, data) => {
    //       return (
    //         <Button
    //           color="secondary"
    //           variant="outlined"
    //           type="submit"
    //           onClick={() => {
    //             console.log(
    //               "internData[data.rowIndex",
    //               internData[data.rowIndex]
    //             );
    //             setSelectedItem(internData[data.rowIndex]);
    //             setShowDialog(true);
    //           }}
    //         >
    //           Assign
    //         </Button>
    //       );
    //     },
    //   },
    // },
  ];

  const options = {
    filterType: "dropdown",
    selectableRows: "none",
    download: false,
    print: false,
    textLabels: {
      filter: {
        title: "Filters",
        reset: "Reset",
        apply: "Apply",
      },
    },
  };

  return (
    <>
      <MUIDataTable
        title="Intern List"
        data={internData}
        columns={columns}
        options={options}
        getData={fetchInterns}
      />
      {showDialog && (
        <DialogAssign
          handleClose={handleClose}
          open={showDialog}
          selectedItem={selecteditem}
        />
      )}
    </>
  );
};
