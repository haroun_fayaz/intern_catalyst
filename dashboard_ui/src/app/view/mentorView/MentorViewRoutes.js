import Loadable from '../../components/Loadable';
import { lazy } from 'react';
// import { authRoles } from '../../auth/authRoles';
// import Interns from './interns';

const Layout = Loadable(lazy(() => import('./Layout')));

const MentorViewRoutes = [
  { 
    path: '/mentor_view',
   element: <Layout />, 
  // auth: authRoles.admin
 },
];

export default MentorViewRoutes;