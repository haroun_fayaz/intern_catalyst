import React from "react";
import TabLayout from "../../components/TabLayout";
import { useLocation} from "react-router-dom";


import {AssignedInterns} from "./AssignedInterns";
import Tasks from "./Tasks";


const Layout = () => {

  const location = useLocation();


  const tabs = [
    {
      label: "Interns",
      component: (
        <AssignedInterns />
      ),
    },
    { label: "Tasks", component: <Tasks /> },
  ];

  return (
    <>
      <div className="m-sm-30">
        <TabLayout tabs={tabs} tab={location?.tab} />
      </div>
    </>
  );
};

export default Layout;
