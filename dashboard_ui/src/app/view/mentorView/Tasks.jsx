import React from "react";
import { Button } from "@mui/material";
import { CreateTaskDialog } from "./CreateTaskDialog";
import {UploadDialog } from "./UploadDialog"
import axios from "../../Axios";
import MUIDataTable from "mui-datatables";
import Autocomplete from '@mui/material/Autocomplete';
import TextField from "@mui/material/TextField";
import {localUser} from "../../util"

const Tasks = () => {
  const [showDialog, setShowDialog] = React.useState(false);
  const [showUploadDialog, setShowUploadDialog] = React.useState(false);
  const [taskData, setTaskData] = React.useState([]);
  // const [user, setUser] = React.useState({});
  const [internData, setInterData] = React.useState([]);
  const fetchInterns = async () => {
    try {
      const response = await axios.get("uploads/mentors/", {
        params: {
          // email: "ahtisham.shafi@trialx.com",
          email:localUser().email,
        },
      });
      console.log("intern data", response.data);
      setInterData(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const fetchTasks = async () => {
    try {
      const response = await axios.get("tasks/",{
          params: {
            // mentors: "ahtisham.shafi@trialx.com"
              mentors:localUser().email,
          }
      });
      console.log("data", response.data);
      setTaskData(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  const updateTasks = async (data) => {
    // let id=data.id;
    try {
      const response = await axios.put(`/tasks/${data.id}/`, data);
      console.log("data in update", response.data);
      fetchTasks();
      // setTaskData(response.data);
    } catch (error) {
      console.log("in error")
      console.error(error);
    }
  };
  React.useEffect(() => {
    fetchTasks();
    fetchInterns();
    // setUser(JSON.parse(window.localStorage.getItem("user")));
  }, []);
  const handleUpdate=(internEmail,selectedRow)=>{

    let temp ={
      id:taskData[selectedRow]?.id,
      assigned_to_email:internEmail,
      assigned_by_email:taskData[selectedRow]?.assigned_by.email,
      // completion_date:taskData[selectedRow]?.completion_date,
      completion_date:"2023-02-01",
      title:taskData[selectedRow]?.title,
    }

updateTasks(temp);
  }

  const handleClose = () => {
    setShowDialog(false);
    setShowUploadDialog(false);
    fetchTasks();
    fetchInterns();
  };
  const columns = [
    {
      name: "title",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "status",
      label: "Status",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "assigned_to",
      label: "Assigned To",
      options: {
        filter: true,
        sort: false,
        customBodyRender: (index, data) => {
          console.log("custoom index",index)
          console.log("custoom data",data)
          return (
            <Autocomplete
            name=""
            textfieldprops={{
              label: "assigned",
             
            }}
            value={data?.rowData[2].name}
            options={internData || []}
            onChange={(event, val) => {
              console.log("event in oncahnge",event)
              console.log("data val",val)
              handleUpdate(val.intern_email,data.rowIndex)
              // formik.setFieldValue("driverContact", data);
            }}
            style={{
              flex: "0 0 95%",
            }}
            
            getOptionLabel={(option) => option.name ||data?.rowData[2].name}
            renderInput={(params) => {
              return (
                <TextField {...params} />
              );
            }}
          />
          );
        },
      },
    },
    {
      name: "description",
      label: "Description",
      options: {
        filter: true,
        sort: false,
      },
    },
    {
      name: "completion_date",
      label: "Completion Date",
      options: {
        filter: true,
        sort: false,
      },
    },
  ];
  const options = {
    filterType: "dropdown",
    selectableRows: "none",
    download: false,
    print: false,
  };
  return (
    <>
      <Button
        color="primary"
        variant="contained"
        sx={{ my: 2 }}
        onClick={() => setShowDialog(true)}
      >
        Create
      </Button>
      <Button
        color="primary"
        variant="contained"
        sx={{ my: 2 }}
        onClick={() => setShowUploadDialog(true)}
      >
        Upload
      </Button>
      <MUIDataTable
        title="Intern List"
        data={taskData}
        columns={columns}
        options={options}
        getData={fetchTasks}
      />
      {showDialog && (
        <CreateTaskDialog handleClose={handleClose} open={showDialog} />
      )}
       {showUploadDialog && (
        <UploadDialog handleClose={handleClose} open={showUploadDialog} />
      )}
    </>
  );
};

export default Tasks;
