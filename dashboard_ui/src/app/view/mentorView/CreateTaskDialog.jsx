import React from "react";
import FormDialog from "../../components/FormDialog";
import { Formik, Form } from "formik";
import { Grid } from "@mui/material";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import axios from "../../Axios";
import { Box } from "@mui/material";
import { format } from "date-fns";
import Button from "@mui/material/Button";
import {localUser} from "../../util"
import * as Yup from 'yup';

export const CreateTaskDialog = ({ handleClose, open, selectedItem }) => {
  const [internData, setInterData] = React.useState([]);
  console.log("localUser", localUser())

  const validationSchema = Yup.object().shape({
    completion_date: Yup.date()
      .min(new Date(), 'Date is earlier')
      .nullable()
      // .required('Completion date is required'),
  });
  const fetchInterns = async () => {
    try {
      const response = await axios.get("uploads/mentors/", {
        params: {
          email: localUser().email,
        },
      });
      console.log("data", response.data);
      setInterData(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  React.useEffect(() => {
    fetchInterns();
  }, []);
  const initialValues = {
    title: "",
    status: "to_do",
    assigned_to: "",
    description: "",
    completion_date: format(Date.now(), "yyyy-MM-dd"),
    assigned_by: localUser().email,
    resource:"",
    // assigned_by:window.localStorage.getItem('user')
  };
  const createTask = async (values) => {
    try {
      const response = await axios.post("tasks/", values);
      console.log("File uploaded:", response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <FormDialog
        title="Create Task"
        open={open}
        handleClose={handleClose}
        // fullWidth
      >
        {/* <Box p={4} height="100%"> */}
        <Formik
          onSubmit={(values, actions) => {
            console.log("on submit", values);
            actions.setSubmitting(false);
            handleClose();
            createTask(values);
          }}
          // onSubmit={handleFormSubmit}
          initialValues={initialValues}
          validationSchema={validationSchema}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <Form onSubmit={handleSubmit}>
              <Box sx={{ display: "flex", flexDirection: "column" ,justifyContent: "space-between"}}>
                {/* <TextField label="Name" name="title" 
                       sx={{ mt:"10px" }}
               />
                   <TextField
                    label="Completion Date"
                    name="completion_date"
                    type="date"
                    // value={date}
                    // onChange={handleDateChange}
                    // sx={{ width: 220 }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    sx={{ mt:"10px" }}
                  /> */}
                <Grid container spacing={4}>
                  <Grid sm={12} xs={12}>
                    <TextField
                      label="Title"
                      name="title"
                      sx={{ mt: "10px" }}
                      fullWidth
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid sm={6} xs={12}>
                    <TextField
                      // id="outlined-select-currency"
                      select
                      name="assigned_to_email"
                      label="Assign To"
                      fullWidth
                      defaultValue=""
                      sx={{ height: "30px", mt: "10px" }}
                      onChange={handleChange}
                      // helperText="Assign To Intern"
                    >
                      {internData.map((option) => (
                        <MenuItem key={option.name} value={option.intern_email}>
                          {option.name}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  {/* <Grid sm={6} xs={12}>
                    <TextField
                      // id="outlined-select-currency"
                      select
                      label="Status"
                      name="status"
                      fullWidth
                      defaultValue="to_do"
                      sx={{ height: "30px", mt: "10px" }}
                      onChange={handleChange}

                      // helperText="Assign To Intern"
                    >
                      {TASK_STATUS.map((option) => (
                        <MenuItem key={option} value={option}>
                          {option}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid> */}
                  <Grid sm={6} xs={12}>
                    <TextField
                      label="Completion Date"
                      name="completion_date"
                      type="date"
                      format="yyyy-MM-dd"
                      onChange={handleChange}
                      fullWidth
                      error={!!errors.completion_date}
                      helperText={errors.completion_date}
                      defaultValue={format(Date.now(), "yyyy-MM-dd")}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      sx={{ height: "30px", mt: "10px" }}
                    />
                  </Grid>
                  <Grid sm={12} xs={12}>
                    <TextField
                      label="Description"
                      name="description"
                      fullWidth
                      multiline
                      rows={3}
                      sx={{ mt: "50px" }}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid xs={12}>
                    <Button
                      type="submit"
                      color="primary"
                      variant="contained"
                      sx={{ mt: "10px" }}
                    >
                      Submit
                    </Button>
                  </Grid>
                </Grid>
              </Box>
            </Form>
          )}
        </Formik>
        {/* </Box> */}
      </FormDialog>
    </>
  );
};

