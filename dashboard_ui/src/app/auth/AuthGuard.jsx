import useAuth from "../hooks/useAuth";
// import { flat } from 'app/utils/utils';
import { useNavigate, useLocation, redirect, Navigate } from "react-router-dom";

// import AllPages from '../routes';

// const userHasPermission = (pathname, user, routes) => {
//   if (!user) {
//     return false;
//   }
//   const matched = routes.find((r) => r.path === pathname);

//   const authenticated =
//     matched && matched.auth && matched.auth.length ? matched.auth.includes(user.role) : true;
//   return authenticated;
// };

const AuthGuard = ({ children }) => {
  const accessToken = window.localStorage.getItem("accessToken");
  const isAuthenticated =
    accessToken && accessToken !== undefined && accessToken !== "";
  const user = JSON.parse(window.localStorage.getItem("user"));
  const navigate = useNavigate();

  const { pathname } = useLocation();

  //   const routes = flat(AllPages);

  //   const hasPermission = userHasPermission(pathname, user, routes);
  //   let authenticated = isAuthenticated && hasPermission;

  // // IF YOU NEED ROLE BASED AUTHENTICATION,
  // // UNCOMMENT ABOVE LINES
  // // AND COMMENT OUT BELOW authenticated VARIABLE

  let authenticated = isAuthenticated;

  return (
    <>
      {authenticated && user ? (
        pathname?.includes("upload") ? (
          user?.is_superuser ? (
            children
          ) : user?.is_intern ? (
            <Navigate replace to="/intern_view" state={{ from: pathname }} />
          ) : (
            <Navigate replace to="mentor_view" state={{ from: pathname }} />
          )
        ) : (
          children
        )
      ) : (
        <Navigate replace to="/session/signin" state={{ from: pathname }} />
      )}
    </>
  );
};

export default AuthGuard;
