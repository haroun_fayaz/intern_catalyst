import React, { useState, useEffect } from "react";
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

export default function TabLayout({ tabs, tab }) {
  const [tabIndex, setTabIndex] = useState(0);
  useEffect(() => {
    if (tab) {
      setTabIndex(tab);
    }
  }, [tab]);
  return (
    <>
      <Tabs
        className="my-3"
        value={tabIndex}
        onChange={(_e, value) => setTabIndex(value)}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons={true}
      >
        {tabs.map((item, ind) => (
          <Tab
            className="capitalize"
            value={ind}
            label={item.label}
            key={ind}
            sx={{ marginRight: '16px' }}
          />
        ))}
      </Tabs>
      {tabs[tabIndex].component}
    </>
  );
}
