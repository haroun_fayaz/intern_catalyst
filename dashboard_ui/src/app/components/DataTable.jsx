import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';



export default function DataTable({rows,columns,title}) {
  return (
    <div style={{ height: 400, width: '100%' }}>
        <h4>{title}</h4>
      <DataGrid
        rows={rows}
        columns={columns}
        paginationModel={{ page: 0, pageSize: 5 }}
        checkboxSelection
      />
    </div>
  );
}