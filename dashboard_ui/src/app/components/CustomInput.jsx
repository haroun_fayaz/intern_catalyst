import React from "react";
import { useField } from "formik";
import { TextField } from "@mui/material";

export default function CustomInput(props) {
  const [field, meta] = useField(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      className="mb-4"
      fullWidth
      size="small"
      variant="outlined"
      {...field}
      helperText={errorText}
      error={!!errorText}
      {...props}
    />
  );
}