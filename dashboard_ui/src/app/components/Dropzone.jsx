import React from "react";
import { DropzoneArea } from "react-mui-dropzone";
import UploadFileIcon from "@mui/icons-material/UploadFile";

function DropzoneAreaExample({
  dropzoneText = "Drag and drop a file here or click",
  Icon,
  dropzoneClass,
  ...props
}) {
  return (
    <DropzoneArea
      dropzoneClass={dropzoneClass}
      onChange={(e) => console.log(e)}
      dropzoneText={dropzoneText}
      Icon={Icon || UploadFileIcon}
      {...props}
    />
  );
}

export default DropzoneAreaExample;
