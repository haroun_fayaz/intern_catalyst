import * as React from "react";
import { Dialog, DialogTitle, IconButton, Divider } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import DialogContent from '@mui/material/DialogContent';

export default function FormDialog({
  handleClose,
  open,
  title,
  children,
  maxWidth,
  ...others
}) {
  return (
    <Dialog
      onClose={handleClose}
      open={open}
      maxWidth={maxWidth}
      disableBackdropClick
      {...others}
    >
      <DialogTitle
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        disableTypography
      >
        <h4>{title}</h4>
        <IconButton onClick={handleClose}>
          <CloseIcon color="secondary" />
        </IconButton>
      </DialogTitle>
      <Divider />
      <DialogContent>
      <div style={{padding:"20px",margin:"10px"}}>{children}</div>
        </DialogContent>
      
    </Dialog>
  );
}
