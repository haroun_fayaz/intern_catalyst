import React from "react";
import { useField } from "formik";
import {
  FormControl,
  FormControlLabel,
  FormHelperText,
  Switch,
} from "@mui/material";

export default function CustomSwitch(props) {
  const [field, meta] = useField(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <FormControl>
      <FormControlLabel
        control={<Switch color="primary" {...field} {...props} />}
        label={props.label}
      />
      <FormHelperText>{errorText}</FormHelperText>
    </FormControl>
  );
}
