import AuthGuard from "./auth/AuthGuard";
// import chartsRoute from "app/views/charts/ChartsRoute";
// import dashboardRoutes from "app/views/dashboard/DashboardRoutes";
// import materialRoutes from "app/views/material-kit/MaterialRoutes";

// import applicationRoutes from "app/views/application/applicationRoutes";
import NotFound from "./view/sessions/NotFound";

import { Navigate } from "react-router-dom";
// import Application from "./views/application/Application";
// import Layout from "./components/Layout/Layout";
import sessionRoutes from "./view/sessions/SessionRoutes";
import internRoutes from "./view/intern/InternRoutes";
import UploadRoutes from "./view/upload/UploadRoutes";
import MentorViewRoutes from "./view/mentorView/MentorViewRoutes";
import InternViewRoutes from "./view/internView/InternViewRoutes";
import Navbar from "../app/components/Navbar";
const routes = [
  {
    element: (
      <AuthGuard>
        <Navbar />
      </AuthGuard>
    ),
    children: [
      ...internRoutes,
      ...UploadRoutes,
      ...MentorViewRoutes,
      ...InternViewRoutes,
    ],
  },
  ...sessionRoutes,
  { path: "/", element: <Navigate to="/upload" /> },
  { path: "*", element: <NotFound /> },
];

export default routes;
