// import '../fake-db';
// import { Provider } from 'react-redux';
import { useRoutes } from "react-router-dom";
// import { Theme } from './components';
import { AuthProvider } from "./contexts/JWTAuthContext";
// import { SettingsProvider } from './contexts/SettingsContext';
// import { Store } from './redux/Store';
import routes from "./routes";
import { createTheme, ThemeProvider, styled } from "@mui/material/styles";
import { orange } from "@mui/material/colors";
import theme from "./theme/index";
import Navbar from "../app/components/Navbar";

const App = () => {
  const content = useRoutes(routes);
  return (
    <ThemeProvider theme={theme}>
      <AuthProvider>{content}</AuthProvider>
    </ThemeProvider>
  );
};

export default App;
