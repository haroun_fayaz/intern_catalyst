export const PrimaryLight = "#E7EAFB";
export const Primary = "#0D2ED3";
export const PrimaryMedium = "#4F6DFF";
export const PrimaryDark = "#334dc9";
export const PrimaryBG = "#ffffff";

export const PrimaryContrast = "#ffffff";
export const Error = "#EA4335";
export const Success = "#009856";
export const Warning = "#D89028";
export const Info = "#FFFF00";

export const BaseFontSize = 14;

export const GreyDark = "#dbdbdb";
export const BoxShadow = `0 0.5rem 1rem rgba(${Primary}, 0.15) !default`;

// $box-shadow-sm: 0 0 0.45rem rgba($primary, 0.1) !default;
// $box-shadow: 0 0.5rem 1rem rgba($primary, 0.15) !default;
// $box-shadow-lg: 0 1.1rem 1.71rem rgba($primary, 0.12) !default;

// $primary: #0e4f82 !default;
// $primary-light: #e5ecf1 !default;
// $primary-dark: #004639 !default;
