import { createTheme } from "@mui/material/styles";
import overrides from "./overrides";
import {
  Primary,
  PrimaryLight,
  PrimaryDark,
  PrimaryMedium,
  PrimaryContrast,
  PrimaryBG,
  GreyDark,
  Error,
  Success,
  Warning,
  BaseFontSize,
} from "./constants";

const theme = createTheme({
  palette: {
    primary: {
      main: Primary,
      light: PrimaryLight,
      medium: PrimaryMedium,
      dark: PrimaryDark,
      contrastText: PrimaryContrast,
      bgPrimary: PrimaryBG,
      greyDark: GreyDark,
    },
    secondary: {
      main: PrimaryLight,
    },
    error: {
      main: Error,
    },
    action: {
      disabledBackground: PrimaryContrast,
    },
    background: {
      default: "#ffffff",
    },
    // text: {
    //   primary: Primary,
    // },
    success: {
      main: Success,
    },
    warning: {
      main: Warning,
    },
    contrastThreshold: 3,
    tonalOffset: 0.2,
  },
  typography: {
    fontSize: BaseFontSize,
    fontFamily: "Inter",
    button: {
      textTransform: "none",
    },
    h1: {
      fontSize: BaseFontSize * 3.06,
      fontWeight: 600,
    },
    h2: {
      fontSize: BaseFontSize * 2.44,
      fontWeight: 600,
    },
    h3: {
      fontSize: BaseFontSize * 1.94,
      fontWeight: 600,
    },
    h4: {
      fontSize: BaseFontSize * 1.56,
      fontWeight: 600,
    },
    h5: {
      fontSize: BaseFontSize * 1.25,
      fontWeight: 600,
    },
    h6: {
      fontSize: BaseFontSize,
    },
    title: {
      fontSize: BaseFontSize * 2.44,
      lineHeight: 1.5,
    },
    subtitle1: {
      fontSize: BaseFontSize * 1.94,
      lineHeight: 1.5,
    },
    subtitle2: {
      fontSize: BaseFontSize * 1.56,
      lineHeight: 1.5,
    },
    // },
    // shape: {
    //   borderRadius: 8,
    // },
    // components: {
    //   MuiTextField: {
    //     defaultProps: {
    //       variant: "standard",
    //       InputLabelProps: {
    //         shrink: true,
    //       },
    //     },
    //   },
    //   MuiButton: {
    //     defaultProps: {
    //       disableRipple: true,
    //       disableFocusRipple: true,
    //     },
    //   },
    //   MuiIconButton: {
    //     defaultProps: {
    //       disableRipple: true,
    //       disableFocusRipple: true,
    //       size: "small",
    //     },
    //   },
    //   MuiCheckbox: {
    //     defaultProps: {
    //       disableRipple: true,
    //       color: "primary",
    //     },
    //   },
    //   MuiRadio: {
    //     defaultProps: { disableRipple: true, color: "primary" },
    //   },
    //   MuiSwitch: {
    //     defaultProps: { color: "primary", disableRipple: true },
    //   },
    //   MuiTab: {
    //     defaultProps: { disableRipple: true, centered: false },
    //   },
  },
});

theme.components = { ...theme.components, ...overrides(theme) };

export default theme;
