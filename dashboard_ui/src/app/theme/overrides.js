import {
  Primary,
  PrimaryLight,
  // PrimaryMedium,
  // PrimaryDark,
  PrimaryContrast,
  GreyDark,
  // PrimaryBG,
} from "./constants";
// import { hexToRgb ,alpha  } from "@mui/material/styles";

// const rgaPrimary = hexToRgb(Primary);
// const withAlphaPrimary = alpha(rgaPrimary, 0.1);

const Override = (theme) => ({
  // // MuiCssBaseline: {
  // //   styleOverrides: {
  // //     "@global": {
  // //       ul: {
  // //         marginTop: "revert",
  // //         marginBottom: "revert",
  // //       },
  // //       label: {
  // //         marginBottom: "unset",
  // //       },
  // //     },
  // //   },
  // // },
  // // MuiButtonBase: {
  // //   styleOverrides: {
  // //     root: {
  // //       borderRadius: "20px",
  // //       // border: "1px solid transparent",
  // //       // "&:focus,&:hover": {
  // //       //   border: "1px solid transparent",
  // //       //   outline: "none",
  // //       // },
  // //     },
  // //   },
  // // },
  // MuiButton: {
  //   styleOverrides: {
  //     root: {
  //       borderRadius: "20px",
  //       textTransform: "none",
  //       fontWeight: "600",
  //     },
  //     contained: {
  //       border: "1px solid transparent",
  //       boxShadow: "none",
  //       "&:hover,&:focus": {
  //         border: "1px solid transparent",
  //         boxShadow: "none",
  //       },
  //       color: PrimaryContrast,
  //     },
  //     textPrimary: {
  //       border: "1px solid transparent",
  //       "&:focus": {
  //         border: "1px solid transparent",
  //         outline: "none",
  //       },
  //       "&:hover": {
  //         backgroundColor: PrimaryLight,
  //       },
  //       "&:active": {
  //         backgroundColor: GreyDark,
  //         border: "1px solid transparent",
  //       },
  //     },
  //     containedPrimary: {
  //       "&:hover": {
  //         backgroundColor: PrimaryLight,
  //       },
  //       "&:active": {
  //         backgroundColor: PrimaryLight,
  //       },
  //     },
  //     startIcon: {
  //       marginRight: 4,
  //     },
  //   },
  // },
  // // MuiOutlinedInput: {
  // //   styleOverrides: {
  // //     root: {
  // //       borderRadius: "4px",
  // //       "&:hover $notchedOutline": {
  // //         borderColor: Primary,
  // //       },
  // //     },
  // //     input: {
  // //       padding: theme.spacing(0.5),
  // //       paddingLeft: theme.spacing(1),
  // //       color: "#3E4953",
  // //     },
  // //     multiline: {
  // //       padding: theme.spacing(0.5),
  // //       paddingLeft: theme.spacing(1),
  // //       color: "#3E4953",
  // //     },
  // //     inputMultiline: {
  // //       padding: theme.spacing(0.5),
  // //       paddingLeft: theme.spacing(1),
  // //     },
  // //   },
  // // },
  // MuiChip: {
  //   styleOverrides: {
  //     root: {
  //       fontWeight: 600,
  //       fontSize: "18px",
  //     },
  //     filled: {
  //       color: PrimaryContrast,
  //     },
  //   },
  // },
  // MuiInputBase: {
  //   styleOverrides: {
  //     root: {
  //       padding: "4px 20px",
  //       borderRadius: "20px",
  //       boxShadow: "-1px -1px #00000029, 0px -1px #00000029",
  //       backgroundColor: PrimaryContrast,
  //       "&:focus-within": {
  //         boxShadow: `-1px -1px ${Primary}, 0px -1px ${Primary}`,
  //       },
  //       "&:hover": {
  //         boxShadow: `-1px -1px ${Primary}, 0px -1px ${Primary}`,
  //       },
  //     },
  //     inputMultiline: {
  //       padding: "4px 20px",
  //     },
  //   },
  // },
  // MuiInput: {
  //   styleOverrides: {
  //     root: {
  //       "&:after": {
  //         borderBottom: "none",
  //       },
  //     },
  //     underline: {
  //       "&:before": {
  //         borderBottom: "none",
  //       },
  //       "&.focus:not(.Mui-disabled):before": {
  //         borderBottom: "none",
  //       },
  //       "&:hover:not(.Mui-disabled):before": {
  //         borderBottom: "none",
  //       },
  //       "&.Mui-disabled:before": {
  //         borderBottomStyle: "none",
  //       },
  //     },
  //   },
  // },
  // // MuiFormHelperText: {
  // //   styleOverrides: {
  // //     root: {
  // //       position: "absolute",
  // //       bottom: "-17px",
  // //     },
  // //   },
  // // },
  // // MuiFilledInput: {
  // //   styleOverrides: {
  // //     root: {
  // //       backgroundColor: PrimaryContrast,
  // //       padding: theme.spacing(0.5),
  // //       paddingLeft: theme.spacing(1),
  // //       color: "#3E4953",
  // //       borderTopLeftRadius: 0,
  // //       borderTopRightRadius: 0,
  // //     },
  // //     underline: {
  // //       "&:before": {
  // //         borderBottom: "none",
  // //       },
  // //       "&.Mui-disabled:before": {
  // //         borderBottomStyle: "none",
  // //       },
  // //     },
  // //     input: {
  // //       padding: "6px 0 7px",
  // //     },
  // //   },
  // // },
  // // MuiAutocomplete: {
  // //   styleOverrides: {
  // //     inputRoot: {
  // //       background: PrimaryContrast,
  // //       padding: "4px 4px",
  // //     },
  // //     input: {
  // //       // leftPadding: theme.spacing(1),
  // //     },
  // //   },
  // // },
  // // MuiIconButton: {
  // //   styleOverrides: {
  // //     root: {
  // //       border: "1px solid transparent",
  // //       borderRadius: 4,
  // //       "&:focus": {
  // //         border: "1px solid transparent",
  // //         outline: "none",
  // //       },
  // //     },
  // //     colorPrimary: {
  // //       "&:hover": {
  // //         backgroundColor: "transparent",
  // //       },
  // //     },
  // //   },
  // // },
  // MuiFormLabel: {
  //   styleOverrides: {
  //     root: {
  //       marginBottom: "8px",
  //       fontWeight: 600,
  //       color: Primary,
  //       "&:hover": {
  //         color: Primary,
  //       },
  //     },
  //   },
  // },
  // // MuiListItem: {
  // //   styleOverrides: {
  // //     container: {
  // //       listStyle: "none",
  // //     },
  // //     root: {
  // //       paddingBottom: 0,
  // //       "&.active": {
  // //         position: "relative",
  // //       },
  // //     },
  // //     button: {
  // //       "&:hover": {
  // //         backgroundColor: "#e5ecf1",
  // //       },
  // //     },
  // //   },
  // // },
  // // MuiList: {
  // //   styleOverrides: {
  // //     root: {
  // //       padding: theme.spacing(1),
  // //     },
  // //   },
  // // },
  // MuiListItemIcon: {
  //   styleOverrides: {
  //     root: {
  //       justifyContent: "center",
  //     },
  //   },
  // },
  // MuiListItemButton: {
  //   styleOverrides: {
  //     root: {
  //       justifyContent: "center",
  //     },
  //   },
  // },
  // // MuiPaper: {
  // //   styleOverrides: {
  // //     elevation2: {
  // //       boxShadow: `0 0.5rem 1rem ${withAlphaPrimary}`,
  // //     },
  // //     elevation1: {
  // //       boxShadow: "none",
  // //     },
  // //   },
  // // },
  // // MuiTab: {
  // //   styleOverrides: {
  // //     wrapper: {
  // //       alignItems: "flex-start",
  // //     },
  // //     selected: {},
  // //     root: {
  // //       fontWeight: 400,
  // //       lineHeight: 1.5,
  // //       [theme.breakpoints.up("sm")]: {
  // //         minWidth: 80,
  // //       },
  // //     },
  // //   },
  // // },
  // // MuiTabs: {
  // //   styleOverrides: {
  // //     indicator: {
  // //       display: "flex",
  // //       justifyContent: "flex-start",
  // //     },
  // //   },
  // // },
  // MuiTypography: {
  //   styleOverrides: {
  //     root: {
  //       color: Primary,
  //       wordBreak: "break-word",
  //     },
  //   },
  // },
});

export default Override;
